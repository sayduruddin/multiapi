import React from 'react'

export default function MemeHeader() {

    return (
        <div className='flex justify-between p-5 items-center bg-gray-700 text-gray-50'>
            <h1 className='text-3xl'>Meme Generator</h1>
            <h2 className='text-2xl'>From Bob Ziroll's React Course</h2>
        </div>
    )
}
