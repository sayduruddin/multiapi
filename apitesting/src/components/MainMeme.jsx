import React from 'react';
import MemeHeader from '../components/MemeHeader';
import MemeComp from './MemeComp';

export default function MainMeme(props) {

    return (
        <div>
            <MemeHeader />
            <MemeComp />
        </div>
    )
}
