import React from "react";

export default function MemeComp() {
	function generateMeme() {
		console.log("Meme generated");
	}

	return (
		<div>
			<main>
				<div className='grid grid-rows-2 grid-cols-6 gap-3 p-5 justify-center items-center'>
					<label html='topLine' className='col-span-1'>
						Top Line
					</label>
					<input
						type='text'
						className='border border-gray-700 col-span-2 rounded-md p-2'
						id='topLine'
					/>
					<label htmlFor='bottomLine' className='col-span-1'>
						Bottom Line
					</label>
					<input
						type='text'
						className='border border-gray-700 col-span-2 rounded-md p-2'
						id='bottomLine'
					/>

					<button
						className='border border-gray-700 col-span-6 p-2 rounded-lg hover:italic hover:font-semibold'
						onClick={generateMeme}>
						Generate a new meme image
					</button>
				</div>
			</main>
		</div>
	);
}
