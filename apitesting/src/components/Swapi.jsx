import React, { useState, useEffect } from 'react'



// In this component, we are going to attempt to use the useEffect hook 
// and fetch data from the star wars api.

export default function Swapi() {

    const [data, setData] = useState(null);
    const [value, setValue] = useState(1);

    useEffect(() => {
        // Going to be using fetch to the API URL

        fetch(`https://swapi.dev/api/people/${value}/`)
            .then( (response) => response.json())
            .then( (response) => setData(response) );

    }, [value]);

    // Need to modify so that users can input in their own id and then I can have it as a dependancy and it will reload the data
    // from the API.
    function increment() {
        setValue(value => value + 1)
    }

    function decrement() {
        setValue(value => value - 1);
    }

    // console.log(data);
    // Can maybe update below to use conditional rendering such as: {data && someComponent}
    if (data) {
        return (
            <div>
                <h1>Hello from Swapi</h1>
                <div>
                    <p>The value of the character: {value}</p>
                    <p>The character we have got from API call: {data.name}</p>
                    <p>Update the number: <button onClick={ increment }>Click to update +1</button>
                                          <button onClick={ decrement }>Click to update -1</button>
                    </p>
                </div>
            </div>
        )
    }
    else {

        return (
            <div>
                <h1>Hello from Swapi</h1>
                <div>
                    <p>The character we have got from API call: loading</p>
    
                </div>
            </div>
        )
    }
    
   
}
